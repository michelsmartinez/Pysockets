import socket
import sys
 
BUFFER_SIZE = 1024
MSG_FROM_SERVER = "O servidor está vivoooo"
host = '192.168.1.207' 
port = 8003
 
 
def create_server():
    """ 
    Create a server that listen on port 8003 and send response 
    to client using tcp conection
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Cria o descritor do socket
    
    sock.bind((host, port)) # Associa o endereço e porta ao descritor do socket.
    sock.listen(10) # Tamanho maximo da fila de conexões pendentes
 
    print("Tcp server wait Conections on port 8003...")
 
    while True:
        try:
            (con, address) = sock.accept() # aceita conexoes e recupera o endereco do cliente.
            print(address[0]+" Conected...")
            data = con.recv(BUFFER_SIZE) # Recebe uma mensagem do tamanho BUFFER_SIZE
            if len(str(data)) >= 0:
                print(address[0]+" say: " + data.decode('UTF-8'))
                print("Response: " + MSG_FROM_SERVER)
                con.send(MSG_FROM_SERVER.encode('utf-8')) # Envia mensagem através do socket.
            else:
                print("No data received from: "+address[0])
        except ValueError:
            print("Error: Accept")
 
 
if __name__== "__main__":
    create_server()
